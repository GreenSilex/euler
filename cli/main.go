package main

import (
	"flag"
	"fmt"
	"gitlab.com/greensilex/euler/problem"
	"gitlab.com/greensilex/zap_log_wrapper"
	"go.uber.org/zap"
	"runtime"
	"sync"
)

var (
	logger *zap.SugaredLogger
	wg     sync.WaitGroup
	level  = "info"
)

func init() {
	flag.StringVar(&level, "log-level", level, "potential values: debug, info, warn, error")
}

func main() {
	fmt.Println("vim-go")

	runtime.GOMAXPROCS(2) // set the number of CPUs to use
	var problemNum int
	flag.IntVar(&problemNum, "problem", 0, "potential values: 1, 12")

	flag.Parse()

	logger := greensilexZapWrapper.LogSetup(level)
	logger.Debug("in main function")

	switch {
	case problemNum == 0:
		logger.Info("No problem selected")
	case problemNum == 12:
		problem.Problem012(logger)
	}
}
