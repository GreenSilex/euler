/***
Find the divisors of a number

Pass in a number via a channel

Send the divisors via a slice over a channel
***/

package lib

import (
	"go.uber.org/zap"
	"math"
)

// setup logging
var logger *zap.SugaredLogger

func Divisor(logger *zap.SugaredLogger, num chan int64, divisorSliceChan chan []int64) {
	logger.Debug("starting Divisor")

	var i, tmpNum, tmpNum2 int64
	var sqrtNum float64
	var tmpSlice []int64

	for {
		tmpNum = <-num
		logger.Debug("Triangle number: ", tmpNum)
		sqrtNum = math.Sqrt(float64(tmpNum))
		logger.Debug("Square root number", sqrtNum)

		// check if number is a whole number
		if sqrtNum == float64(int64(sqrtNum)) {
			// if our number is divisable by the square root, we just want to add that number once, not twice
			if tmpNum%int64(sqrtNum) == 0 {
				tmpSlice = append(tmpSlice, int64(sqrtNum))
				sqrtNum--
			}
		}
		tmpSlice = nil // we need to start w/ a clean slice for each loop
		// add the original number as the first value in the slice

		tmpSlice = append(tmpSlice, tmpNum)
		// now loop through the remaining numbers, check for divisibility, and
		// add them both to the slice
		for i = int64(sqrtNum); i > 0; i-- {
			if tmpNum%i == 0 {
				tmpNum2 = tmpNum / i
				logger.Debug("tmpnum2 = ", tmpNum2, " i = ", i)
				tmpSlice = append(tmpSlice, i)
				tmpSlice = append(tmpSlice, tmpNum2)
			}
		}

		// put the created slice on the channel
		logger.Debug("divisor slice: ", tmpSlice)
		divisorSliceChan <- tmpSlice
	}
}
