/***
Create Triangle numbers

Pass in a channel and the numbers will be returned on that channel
***/

package lib

import (
	"go.uber.org/zap"
	"sync"
)

//setup logging
//var logger *zap.SugaredLogger

func TriangleNumber(logger *zap.SugaredLogger, num chan int64, wg *sync.WaitGroup) {
	defer wg.Done()

	logger.Debug("staring TriangleNumber")
	var i, triNum int64
	i = 1
	triNum = 1

	for {
		i++
		triNum = triNum + i
		num <- triNum
	}
}
