// zap is a very quick logger for go
// this is a wrapper for using colored logging and outputting non-json logs

package greensilexZapWrapper

import "go.uber.org/zap"
import "go.uber.org/zap/zapcore"

// Use this function to setup logging in other packages
// import package:      "go.uber.org/zap"
// setup variable:      var logger *zap.SugaredLogger
// change log levels:   logger := greensilexZapWrapper.LogSetup(level)
func LogSetup(logLevel string) *zap.SugaredLogger {
	prelogger, _ := Log(logLevel)
	LogIt := prelogger.Sugar()
	return LogIt
}

// Change log levels
func Log(custom_level string) (*zap.Logger, error) {
	var custom_log_level zapcore.Level
	if custom_level == "info" {
		custom_log_level = zap.InfoLevel
	} else if custom_level == "debug" {
		custom_log_level = zap.DebugLevel
	} else if custom_level == "warn" {
		custom_log_level = zap.WarnLevel
	} else if custom_level == "error" {
		custom_log_level = zap.ErrorLevel
	} else {
		custom_log_level = zap.InfoLevel
	}
	return LogColorConfig(custom_log_level).Build()
}

// Levels: InfoLevel, WarnLevel, ErrorLevel, DPanicLevel, PanicLevel, FatalLevel
// Stacktraces are automatically included on logs of ErrorLevel and above.
func LogColorConfig(custom_log_level zapcore.Level) zap.Config {

	return zap.Config{
		Level:       zap.NewAtomicLevelAt(custom_log_level),
		Development: true,
		//Sampling: &zap.SamplingConfig{
		//	Initial:    100,
		//	Thereafter: 100,
		//},
		// json or console
		Encoding:      "console",
		EncoderConfig: LogColorEncoderConfig(),
		//TODO allow writing logs to disk taking input from app
		//OutputPaths:      []string{"stdout", "/tmp/logs"},
		OutputPaths:      []string{"stdout"},
		ErrorOutputPaths: []string{"stderr"},
	}
}

func LogColorEncoderConfig() zapcore.EncoderConfig {
	return zapcore.EncoderConfig{
		TimeKey:  "ts",
		LevelKey: "level",
		NameKey:  "logger",
		// This lists the line that calls the log
		//		CallerKey:      "caller",
		MessageKey:    "msg",
		StacktraceKey: "stacktrace",
		EncodeLevel:   zapcore.CapitalColorLevelEncoder,
		// ISO8601TimeEncoder or EpochTimeEncoder ; EpochTimeEncoder breaks in console encoding
		EncodeTime:     zapcore.ISO8601TimeEncoder,
		EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	}
}
