PROJECT_NAME := "euler"
PKG := "gitlab.com/greensilex/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)
WORK_DIR := "/builds/GreenSilex/euler"
BUILD_DIR := "$(WORK_DIR)/build"

.PHONY: all build

all: build

# Get the dependencies
dep: ; @dep ensure -update

# Build the binary file
build: dep ; @go build -i -v -o "$(WORK_DIR)/$(PROJECT_NAME)" ./cli/main.go

# Display this help screen
help: ; @grep -h -E '^[a-zA-Z_-]+:.*?# .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
